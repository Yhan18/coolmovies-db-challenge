import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'app/config/config.dart';
import 'app/presenter/core/app_widget.dart';
void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Future.wait([
    Config.instance.call(),
  ]);

  runApp(
    GraphQLProvider(
      client: Config.instance.graphQlClient,
      child: const AppWidget(),
    ),
  );
}