import 'package:flutter/material.dart';

import '../../domain/entities/movies_entity.dart';

@immutable
class MoviesDto {
  final String id;
  final String imgUrl;
  final String movieDirectorId;
  final String userCreatorId;
  final String title;
  final String releaseDate;
  final String nodeId;
  final UserByUserCreatorIdDto userByUserCreatorId;
  final MovieDirectorByMovieDirectorIdDto director;

  const MoviesDto({
    required this.id,
    required this.imgUrl,
    required this.movieDirectorId,
    required this.userCreatorId,
    required this.title,
    required this.releaseDate,
    required this.nodeId,
    required this.userByUserCreatorId,
    required this.director,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'imgUrl': imgUrl,
      'movieDirectorId': movieDirectorId,
      'userCreatorId': userCreatorId,
      'title': title,
      'releaseDate': releaseDate,
      'nodeId': nodeId,
      'userByUserCreatorId': userByUserCreatorId.toMap(),
      'movieDirectorByMovieDirectorId': director.toMap(),
    };
  }

  factory MoviesDto.fromMap(Map<String, dynamic> map) {
    return MoviesDto(
      id: map['id'] ?? '',
      imgUrl: map['imgUrl'] ?? '',
      movieDirectorId: map['movieDirectorId'] ?? '',
      userCreatorId: map['userCreatorId'] ?? '',
      title: map['title'] ?? '',
      releaseDate: map['releaseDate'] ?? '',
      nodeId: map['nodeId'] ?? '',
      userByUserCreatorId: UserByUserCreatorIdDto.fromMap(
        map['userByUserCreatorId'],
      ),
      director: MovieDirectorByMovieDirectorIdDto.fromMap(
        map['movieDirectorByMovieDirectorId'],
      ),
    );
  }

  MoviesEntity toEntity() => MoviesEntity(
      id: id,
      imgUrl: imgUrl,
      movieDirectorId: movieDirectorId,
      userCreatorId: userCreatorId,
      title: title,
      releaseDate: releaseDate,
      nodeId: nodeId,
      userByUserCreatorId: userByUserCreatorId.toEntity(),
      director: director.toEntity());

  factory MoviesDto.fromEntity(MoviesEntity entity) => MoviesDto(
        id: entity.id,
        imgUrl: entity.imgUrl,
        movieDirectorId: entity.movieDirectorId,
        userCreatorId: entity.userCreatorId,
        title: entity.title,
        releaseDate: entity.releaseDate,
        nodeId: entity.nodeId,
        userByUserCreatorId: UserByUserCreatorIdDto.fromEntity(
          entity.userByUserCreatorId,
        ),
        director: MovieDirectorByMovieDirectorIdDto.fromEntity(entity.director),
      );
}

@immutable
class UserByUserCreatorIdDto {
  final String id;
  final String name;
  final String nodeId;

  const UserByUserCreatorIdDto({
    required this.id,
    required this.name,
    required this.nodeId,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'nodeId': nodeId,
    };
  }

  factory UserByUserCreatorIdDto.fromMap(Map<String, dynamic> map) {
    return UserByUserCreatorIdDto(
      id: map['id'] ?? '',
      name: map['name'] ?? '',
      nodeId: map['nodeId'] ?? '',
    );
  }

  UserByUserCreatorId toEntity() => UserByUserCreatorId(
        id: id,
        name: name,
        nodeId: nodeId,
      );

  factory UserByUserCreatorIdDto.fromEntity(UserByUserCreatorId entity) =>
      UserByUserCreatorIdDto(
        id: entity.id,
        name: entity.name,
        nodeId: entity.nodeId,
      );
}

@immutable
class MovieDirectorByMovieDirectorIdDto {
  final String id;
  final String name;
  final int age;

  const MovieDirectorByMovieDirectorIdDto({
    required this.id,
    required this.name,
    required this.age,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'age': age,
    };
  }

  factory MovieDirectorByMovieDirectorIdDto.fromMap(Map<String, dynamic> map) {
    return MovieDirectorByMovieDirectorIdDto(
      id: map['id'] ?? '',
      name: map['name'] ?? '',
      age: map['age']?.toInt() ?? 0,
    );
  }

  MovieDirectorByMovieDirectorId toEntity() => MovieDirectorByMovieDirectorId(
        id: id,
        name: name,
        age: age,
      );

  factory MovieDirectorByMovieDirectorIdDto.fromEntity(
    MovieDirectorByMovieDirectorId entity,
  ) =>
      MovieDirectorByMovieDirectorIdDto(
        age: entity.age,
        id: entity.id,
        name: entity.name,
      );
}
