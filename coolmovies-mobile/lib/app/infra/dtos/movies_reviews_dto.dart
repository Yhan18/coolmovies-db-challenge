import '../../domain/entities/movies_reviews.dart';

class MovieReviewsDto {
  final String id;
  final String title;
  final String body;
  final int rating;
  final UserByUserReviewerIdDto userByUserReviewerId;

  const MovieReviewsDto({
    required this.id,
    required this.title,
    required this.body,
    required this.rating,
    required this.userByUserReviewerId,
  });

  MovieReviewsDto copyWith({
    String? id,
    String? title,
    String? body,
    int? rating,
    UserByUserReviewerIdDto? userByUserReviewerId,
  }) {
    return MovieReviewsDto(
      id: id ?? this.id,
      title: title ?? this.title,
      body: body ?? this.body,
      rating: rating ?? this.rating,
      userByUserReviewerId: userByUserReviewerId ?? this.userByUserReviewerId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'body': body,
      'rating': rating,
      'userByUserReviewerId': userByUserReviewerId.toMap(),
    };
  }

  factory MovieReviewsDto.fromMap(Map<String, dynamic> map) {
    return MovieReviewsDto(
      id: map['id'] ?? '',
      title: map['title'] ?? '',
      body: map['body'] ?? '',
      rating: map['rating']?.toInt() ?? 0,
      userByUserReviewerId:
          UserByUserReviewerIdDto.fromMap(map['userByUserReviewerId']),
    );
  }

  MovieReviewsEntity toEntity() => MovieReviewsEntity(
        id: id,
        title: title,
        body: body,
        rating: rating,
        userByUserReviewerId: userByUserReviewerId.toEntity(),
      );
}

class UserByUserReviewerIdDto {
  final String name;
  final String id;

  const UserByUserReviewerIdDto({
    required this.name,
    required this.id,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'id': id,
    };
  }

  factory UserByUserReviewerIdDto.fromMap(Map<String, dynamic> map) {
    return UserByUserReviewerIdDto(
      name: map['name'] ?? '',
      id: map['id'] ?? '',
    );
  }

  UserByUserReviewerIdEntity toEntity() =>
      UserByUserReviewerIdEntity(name: name, id: id);
}
