import 'package:dartz/dartz.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../domain/entities/movies_entity.dart';
import '../../domain/entities/movies_reviews.dart';
import '../../domain/services/movie_service.dart';
import '../../resources/failures/common_failures.dart';
import '../dtos/movies_dto.dart';
import '../dtos/movies_reviews_dto.dart';

class MovieServiceImpl implements MovieService {
  final GraphQLClient client;

  MovieServiceImpl(this.client);

  @override
  Future<Either<CommonFailures, List<MoviesEntity>>> fetchMovies() async {
    try {
      final QueryResult result = await client.query(QueryOptions(
        document: gql(r'''
          query AllMovies {
            allMovies {
              nodes {
                id
                imgUrl
                movieDirectorId
                userCreatorId
                title
                releaseDate
                nodeId
                movieDirectorByMovieDirectorId {
       						age
        					id
        					name
								}
                userByUserCreatorId {
                  id
                  name
                  nodeId
                }
              }
            }
          }
        '''),
      ));

      if (result.hasException || result.data == null) {
        return left(CommonFailures.unknown);
      }

      final List<MoviesEntity> movieList =
          result.data!['allMovies']['nodes'].map<MoviesEntity>((movie) {
        return MoviesDto.fromMap(movie as Map<String, dynamic>).toEntity();
      }).toList();

      return right(movieList);
    } catch (_) {
      return left(CommonFailures.unknown);
    }
  }

  @override
  Future<Either<CommonFailures, List<MovieReviewsEntity>>> fetchReviews(
    String movieId,
  ) async {
    try {
      final QueryResult result = await client.query(QueryOptions(
        document: gql(
          '''
         {
          allMovieReviews(
            filter: {movieId: {equalTo: "$movieId"}}
          ) {
            nodes {
              id
              title
              body
              rating
              movieByMovieId {
                title
              }
              userByUserReviewerId {
                name
                id
              }
            }
          }
        }
        ''',
        ),
        fetchPolicy: FetchPolicy.noCache,
        cacheRereadPolicy: CacheRereadPolicy.ignoreAll,
      ));

      if (result.hasException || result.data == null) {
        return left(CommonFailures.unknown);
      }

      final List<MovieReviewsEntity> movieList = result.data!['allMovieReviews']
              ['nodes']
          .map<MovieReviewsEntity>((movie) {
        return MovieReviewsDto.fromMap(movie as Map<String, dynamic>)
            .toEntity();
      }).toList();

      return right(movieList);
    } catch (_) {
      return left(CommonFailures.unknown);
    }
  }

  @override
  Future<Either<CommonFailures, Unit>> createComment({
    required String movieId,
    required String userId,
    required double rating,
    required String title,
    required String body,
  }) async {
    try {
      final QueryResult result = await client.query(
        QueryOptions(
          document: gql('''
        mutation {
          createMovieReview(input: {
            movieReview: {
              title: "$title",
              body: "$body",
              rating: ${rating.toInt()}
              movieId: "$movieId",
              userReviewerId: "$userId"
            }})
          {
            movieReview {
              id
              title
              body
              rating
              movieByMovieId {
                title
              }
              userByUserReviewerId {
                name
              }
            }
          }
        }
        '''),
        ),
      );

      if (result.hasException || result.data == null) {
        return left(CommonFailures.unknown);
      }
      return right(unit);
    } catch (_) {
      return left(CommonFailures.unknown);
    }
  }

  @override
  Future<Either<CommonFailures, Unit>> deleteComment(String commentId) async {
    try {
      final QueryResult result = await client.query(QueryOptions(
        document: gql('''
       mutation {
        deleteMovieReviewById(input: {id: "$commentId"}) {
        deletedMovieReviewId
        }
      }
        '''),
        fetchPolicy: FetchPolicy.noCache,
        cacheRereadPolicy: CacheRereadPolicy.ignoreAll,
      ));

      if (result.hasException || result.data == null) {
        return left(CommonFailures.unknown);
      }

      return right(unit);
    } catch (_) {
      return left(CommonFailures.unknown);
    }
  }
}
