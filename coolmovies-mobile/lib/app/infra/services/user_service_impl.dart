import 'package:dartz/dartz.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../domain/services/user_service.dart';

class UserServiceImpl implements UserService {
  final GraphQLClient client;
  UserServiceImpl({
    required this.client,
  });

  @override
  Future<Option<String>> setCurrentUser() async {
    try {
      final QueryResult result = await client.query(QueryOptions(
        document: gql(r'''
          query {
            currentUser {
              id
              name
            }
          }
        '''),
      ));

      if (result.hasException || result.data == null) {
        return none();
      }

      final data = result.data!['currentUser'];

      final prefs = await SharedPreferences.getInstance();
      if (!prefs.containsKey('userId')) {
        prefs.setString('userId', data['id'].toString());
      }

      return some(data['id']);
    } catch (_) {
      return none();
    }
  }

  @override
  Future<Option<String>> getCurrentUser() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final userId = prefs.getString('userId');
      return some(userId ?? '');
    } catch (_) {
      return none();
    }
  }
}
