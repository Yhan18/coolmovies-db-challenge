import 'package:equatable/equatable.dart';

class MoviesEntity extends Equatable {
  final String id;
  final String imgUrl;
  final String movieDirectorId;
  final String userCreatorId;
  final String title;
  final String releaseDate;
  final String nodeId;
  final UserByUserCreatorId userByUserCreatorId;
  final MovieDirectorByMovieDirectorId director;

  const MoviesEntity({
    required this.id,
    required this.imgUrl,
    required this.movieDirectorId,
    required this.userCreatorId,
    required this.title,
    required this.releaseDate,
    required this.nodeId,
    required this.userByUserCreatorId,
    required this.director,
  });

  @override
  List<Object?> get props => [
        id,
        imgUrl,
        movieDirectorId,
        userByUserCreatorId,
        title,
        releaseDate,
        nodeId,
        userByUserCreatorId,
        director,
      ];
}

class UserByUserCreatorId extends Equatable {
  final String id;
  final String name;
  final String nodeId;

  const UserByUserCreatorId({
    required this.id,
    required this.name,
    required this.nodeId,
  });

  @override
  List<String> get props => [id, name, nodeId];
}

class MovieDirectorByMovieDirectorId extends Equatable {
  final String id;
  final String name;
  final int age;

  const MovieDirectorByMovieDirectorId({
    required this.id,
    required this.name,
    required this.age,
  });

  @override
  List<Object?> get props => [id, age, name];
}
