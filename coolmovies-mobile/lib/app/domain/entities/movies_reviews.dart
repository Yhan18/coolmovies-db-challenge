import 'package:equatable/equatable.dart';

class MovieReviewsEntity extends Equatable {
  final String id;
  final String title;
  final String body;
  final int rating;
  final UserByUserReviewerIdEntity userByUserReviewerId;

  const MovieReviewsEntity({
    required this.id,
    required this.title,
    required this.body,
    required this.rating,
    required this.userByUserReviewerId,
  });

  @override
  List<Object?> get props => [
        id,
        title,
        body,
        rating,
        userByUserReviewerId,
      ];
}

class UserByUserReviewerIdEntity extends Equatable {
  final String name;
  final String id;

  const UserByUserReviewerIdEntity({required this.name, required this.id});

  @override
  List<Object?> get props => [
        name,
        id,
      ];
}
