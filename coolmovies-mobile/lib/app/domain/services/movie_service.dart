import 'package:dartz/dartz.dart';

import '../../resources/failures/common_failures.dart';
import '../entities/movies_entity.dart';
import '../entities/movies_reviews.dart';

abstract class MovieService {
  Future<Either<CommonFailures, List<MoviesEntity>>> fetchMovies();
  Future<Either<CommonFailures, List<MovieReviewsEntity>>> fetchReviews(
    String movieId,
  );
  Future<Either<CommonFailures, Unit>> createComment({
    required String movieId,
    required String userId,
    required double rating,
    required String title,
    required String body,
  });

  Future<Either<CommonFailures, Unit>> deleteComment(String commentId);
}
