import 'package:dartz/dartz.dart';

abstract class UserService {
  Future<Option<String>> setCurrentUser();
  Future<Option<String>> getCurrentUser();
}
