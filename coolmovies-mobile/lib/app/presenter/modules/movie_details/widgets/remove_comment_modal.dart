import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../resources/shared/theme/app_colors.dart';
import '../../../../resources/shared/theme/app_constraints.dart';
import '../../../../resources/widgets/template_modal.dart';
import '../notifier/delete_comment_notifier.dart';

class RemoveCommentModal extends TemplateModal {
  final String commentId;
  final VoidCallback callback;
  const RemoveCommentModal({
    required this.commentId,
    required this.callback,
    super.key,
  }) : super();

  @override
  Widget buildContent(BuildContext context, WidgetRef ref) {
    final deleteCommentStateNotifierProvider =
        deleteCommentStateNotifierProviderFamily(context);

    final deleteCommentStateNotifier =
        ref.watch(deleteCommentStateNotifierProvider.notifier);

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Confirmation',
          style: GoogleFonts.poppins(
            fontSize: AppConstraints.normal,
            fontWeight: FontWeight.bold,
            color: AppColors.primaryColor,
          ),
        ),
        const SizedBox(
          height: AppConstraints.medium,
        ),
        Text(
          'Do you want to delete this comment?',
          style: GoogleFonts.poppins(
            fontSize: AppConstraints.medium,
            color: AppColors.primaryColor,
          ),
        ),
        const SizedBox(
          height: AppConstraints.medium,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'cancel',
                style: GoogleFonts.poppins(
                  color: AppColors.white,
                ),
              ),
            ),
            const SizedBox(
              width: AppConstraints.medium,
            ),
            ElevatedButton(
              style: const ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(
                AppColors.contrast,
              )),
              onPressed: () async {
                await deleteCommentStateNotifier.removeComment(
                  commentId: commentId,
                );
                callback();
                // ignore: use_build_context_synchronously
                Navigator.of(context).pop(true);
              },
              child: Text(
                'confirm',
                style: GoogleFonts.poppins(
                  color: AppColors.primaryColor,
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}
