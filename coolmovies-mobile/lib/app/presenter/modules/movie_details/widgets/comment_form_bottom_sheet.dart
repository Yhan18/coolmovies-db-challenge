import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../resources/shared/theme/app_colors.dart';
import '../../../../resources/shared/theme/app_constraints.dart';
import '../../../../resources/widgets/template_modal_bottom_sheet.dart';
import '../notifier/add_comment_notifier.dart';

class CommentFormBottomSheet extends TemplateBottomSheet {
  final String movieId;
  final String userId;
  final VoidCallback callback;

  const CommentFormBottomSheet({
    super.key,
    required this.movieId,
    required this.userId,
    required this.callback,
  });

  @override
  Widget buildContent(BuildContext context, WidgetRef ref) {
    final addCommentStateNotifierProvider =
        addCommentStateNotifierProviderFamily(context);

    final addCommentStateNotifier =
        ref.watch(addCommentStateNotifierProvider.notifier);

    final GlobalKey<FormState> formKey = GlobalKey<FormState>();

    final titleController = useTextEditingController(text: '');
    final bodyController = useTextEditingController(text: '');
    final rating = useState<double>(3);

    return Flexible(
      child: ListView(
        shrinkWrap: true,
        children: [
          Text(
            'Add a new comment',
            style: GoogleFonts.poppins(
              color: AppColors.black,
              fontSize: AppConstraints.normal,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.start,
          ),
          Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: const InputDecoration(
                    hintText: 'Add comment title',
                    enabledBorder: OutlineInputBorder(),
                    errorBorder: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'please fill title input';
                    }
                    return null;
                  },
                  controller: titleController,
                ),
                const SizedBox(
                  height: AppConstraints.small,
                ),
                TextFormField(
                  maxLines: 5,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'please fill body input';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    hintText: 'Add comment body',
                    enabledBorder: OutlineInputBorder(),
                    errorBorder: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(),
                  ),
                  controller: bodyController,
                ),
                const SizedBox(
                  height: AppConstraints.small,
                ),
                RatingBar.builder(
                  initialRating: rating.value,
                  minRating: 1,
                  allowHalfRating: true,
                  itemPadding: const EdgeInsets.symmetric(horizontal: 4),
                  itemBuilder: (context, _) => const Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (value) {
                    rating.value = value;
                  },
                ),
                const SizedBox(
                  height: AppConstraints.normal,
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  style: const ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(AppColors.primaryColor),
                  ),
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      await addCommentStateNotifier.addComment(
                        movieId: movieId,
                        userId: userId,
                        rating: rating.value,
                        title: titleController.text,
                        body: bodyController.text,
                      );
                      callback();
                      // ignore: use_build_context_synchronously
                      Navigator.of(context).pop(true);
                    }
                  },
                  child: Text(
                    'Submit',
                    style: GoogleFonts.poppins(color: AppColors.white),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
