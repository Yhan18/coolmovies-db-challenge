import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../domain/services/movie_service.dart';
import '../../../../resources/failures/common_failures.dart';
import '../../../../resources/providers/service_providers.dart';
import '../../../core/common_state/common_state.dart';

typedef DeleteCommentReviewState = CommonState<CommonFailures, Unit>;

class DeleteCommentNotifier extends StateNotifier<DeleteCommentReviewState> {
  final MovieService service;

  DeleteCommentNotifier(this.service)
      : super(const DeleteCommentReviewState.initial());

  Future<void> removeComment({
    required String commentId,
  }) async {
    state = const DeleteCommentReviewState.loadInProgress();

    final result = await service.deleteComment(commentId);

    state = result.fold(DeleteCommentReviewState.loadFailure,
        DeleteCommentReviewState.loadSuccess);
  }
}

final deleteCommentStateNotifierProviderFamily = StateNotifierProvider.family<
    DeleteCommentNotifier, DeleteCommentReviewState, BuildContext>(
  (ref, context) => DeleteCommentNotifier(
    ref.watch(
      movieServiceProvider(GraphQLProvider.of(context).value),
    ),
  ),
);
