import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../domain/services/movie_service.dart';
import '../../../../resources/failures/common_failures.dart';
import '../../../../resources/providers/service_providers.dart';
import '../../../core/common_state/common_state.dart';

typedef AddCommentReviewState = CommonState<CommonFailures, Unit>;

class AddCommentNotifier extends StateNotifier<AddCommentReviewState> {
  final MovieService service;

  AddCommentNotifier(this.service)
      : super(const AddCommentReviewState.initial());

  Future<void> addComment({
    required String movieId,
    required String userId,
    required double rating,
    required String title,
    required String body,
  }) async {
    state = const AddCommentReviewState.loadInProgress();

    final result = await service.createComment(
      movieId: movieId,
      userId: userId,
      rating: rating,
      title: title,
      body: body,
    );

    state = result.fold(AddCommentReviewState.loadFailure, AddCommentReviewState.loadSuccess);
  }
}

final addCommentStateNotifierProviderFamily =
    StateNotifierProvider.family<AddCommentNotifier, AddCommentReviewState, BuildContext>(
  (ref, context) => AddCommentNotifier(
    ref.watch(
      movieServiceProvider(GraphQLProvider.of(context).value),
    ),
  ),
);
