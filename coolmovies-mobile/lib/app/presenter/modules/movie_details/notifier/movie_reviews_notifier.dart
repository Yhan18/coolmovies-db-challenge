import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../domain/entities/movies_reviews.dart';
import '../../../../domain/services/movie_service.dart';
import '../../../../resources/failures/common_failures.dart';
import '../../../../resources/providers/service_providers.dart';
import '../../../core/common_state/common_state.dart';

typedef MovieReviewsState
    = CommonState<CommonFailures, List<MovieReviewsEntity>>;

class MovieReviewsNotifier extends StateNotifier<MovieReviewsState> {
  final MovieService service;

  MovieReviewsNotifier(this.service) : super(const MovieReviewsState.initial());

  Future<void> fetchReviews(String movieId) async {
    state = const MovieReviewsState.loadInProgress();

    final result = await service.fetchReviews(movieId);

    state = result.fold(MovieReviewsState.loadFailure, MovieReviewsState.loadSuccess);
  }
}

final movieReviewsStateNotifierProviderFamily =
    StateNotifierProvider.family<MovieReviewsNotifier, MovieReviewsState, BuildContext>(
  (ref, context) => MovieReviewsNotifier(
    ref.watch(
      movieServiceProvider(GraphQLProvider.of(context).value),
    ),
  ),
);
