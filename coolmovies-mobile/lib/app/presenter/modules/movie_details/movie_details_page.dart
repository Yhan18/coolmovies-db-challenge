import '../../../resources/notifier/user_notifier.dart';
import 'widgets/comment_form_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../domain/entities/movies_entity.dart';
import '../../../resources/shared/theme/app_colors.dart';
import '../../../resources/shared/theme/app_constraints.dart';
import '../../../resources/shared/utils/app_pipes.dart';
import '../../core/args/details_page_args.dart';
import 'notifier/movie_reviews_notifier.dart';
import 'widgets/remove_comment_modal.dart';

class MovieDetailsPage extends HookConsumerWidget {
  final DetailsPageArgs args;
  const MovieDetailsPage({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final movieReviewsStateNotifierProvider =
        movieReviewsStateNotifierProviderFamily(
      context,
    );
    final userServiceStateNotifierProvider =
        userServiceStateNotifierProviderFamily(context);

    final userServiceNotifier =
        ref.watch(userServiceStateNotifierProvider.notifier);

    final movieReviewsStateNotifier =
        ref.watch(movieReviewsStateNotifierProvider.notifier);

    final userId = useState<String>('');

    useEffect(() {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        movieReviewsStateNotifier.fetchReviews(args.movie.id);
        userServiceNotifier.getCurrentUser();
      });

      return null;
    }, const []);

    ref.listen<String>(userServiceStateNotifierProvider, (_, id) {
      userId.value = id;
    });

    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.symmetric(
          horizontal: AppConstraints.medium,
        ),
        children: [
          SafeArea(
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios_new,
                    color: AppColors.white,
                  ),
                ),
                Text(
                  'Movie Details and Reviews',
                  style: GoogleFonts.poppins(
                    fontSize: AppConstraints.medium,
                    color: AppColors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: AppConstraints.medium),
          _MovieInfos(
            movie: args.movie,
          ),
          const SizedBox(height: AppConstraints.medium),
          Consumer(builder: (_, cRef, __) {
            final state = cRef.watch(movieReviewsStateNotifierProvider);
            return state.maybeWhen(
              loadSuccess: (data) {
                return ListView.separated(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: data.length,
                  separatorBuilder: (_, __) {
                    return const Divider();
                  },
                  itemBuilder: (_, index) {
                    final review = data[index];
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  review.title,
                                  style: GoogleFonts.poppins(
                                    color: AppColors.white,
                                    fontSize: AppConstraints.medium,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const SizedBox(height: AppConstraints.xxxsmall),
                                Text(
                                  review.userByUserReviewerId.name,
                                  style: GoogleFonts.poppins(
                                    color: AppColors.white,
                                    fontSize: AppConstraints.small,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            const Spacer(),
                            RatingBarIndicator(
                              rating: review.rating.toDouble(),
                              itemBuilder: (context, index) => const Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              itemSize: 10,
                            ),
                            if (review.userByUserReviewerId.id ==
                                userId.value) ...[
                              const SizedBox(
                                width: AppConstraints.small,
                              ),
                              GestureDetector(
                                onTap: () async {
                                  await RemoveCommentModal(
                                    commentId: review.id,
                                    callback: () {
                                      movieReviewsStateNotifier.fetchReviews(
                                        args.movie.id,
                                      );
                                    },
                                  ).show(context);
                                },
                                child: const CircleAvatar(
                                  radius: 10,
                                  backgroundColor: AppColors.contrast,
                                  child: Icon(
                                    Icons.close,
                                    color: AppColors.primaryColor,
                                    size: 10,
                                  ),
                                ),
                              ),
                            ]
                          ],
                        ),
                        const SizedBox(height: AppConstraints.small),
                        Text(
                          review.body,
                          style: GoogleFonts.poppins(
                            color: AppColors.white,
                            fontSize: AppConstraints.small,
                          ),
                        )
                      ],
                    );
                  },
                );
              },
              loadInProgress: () => const SizedBox(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(
                  color: AppColors.white,
                ),
              ),
              loadFailure: (failure) => Container(
                color: Colors.red,
                width: 200,
                height: 200,
              ),
              orElse: () => const SizedBox.shrink(),
            );
          }),
          const SizedBox(height: AppConstraints.lg),
          ElevatedButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(
                AppColors.contrast,
              ),
            ),
            onPressed: () async {
              await CommentFormBottomSheet(
                movieId: args.movie.id,
                userId: userId.value,
                callback: () {
                  movieReviewsStateNotifier.fetchReviews(
                    args.movie.id,
                  );
                },
              ).show(context);
            },
            child: Text(
              'Add comment',
              style: GoogleFonts.poppins(
                color: AppColors.black,
                fontSize: AppConstraints.medium,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _MovieInfos extends StatelessWidget {
  final MoviesEntity movie;
  const _MovieInfos({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Hero(
              tag: movie.id,
              child: Container(
                margin: const EdgeInsets.only(
                  right: AppConstraints.xsmall,
                ),
                height: 200,
                width: 150,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  image: DecorationImage(
                    image: NetworkImage(
                      movie.imgUrl,
                    ),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(
                      AppConstraints.medium,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: AppConstraints.xlg),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    movie.title,
                    style: GoogleFonts.poppins(
                      color: AppColors.white,
                      fontSize: AppConstraints.medium,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: AppConstraints.small),
                  Text(
                    'Created by: ${movie.director.name}',
                    style: GoogleFonts.poppins(
                      color: AppColors.white,
                      fontSize: AppConstraints.medium,
                    ),
                  ),
                  const SizedBox(height: AppConstraints.small),
                  Text(
                    'Released at: ${AppPipes.formatDate(DateTime.parse(movie.releaseDate))}',
                    style: GoogleFonts.poppins(
                      color: AppColors.white,
                      fontSize: AppConstraints.small,
                    ),
                  ),
                  const SizedBox(height: AppConstraints.small),
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
