import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../resources/failures/common_failures.dart';
import '../../../resources/shared/theme/app_colors.dart';
import '../../../resources/shared/theme/app_constraints.dart';
import '../../../resources/shared/utils/app_pipes.dart';
import '../../core/args/details_page_args.dart';
import '../../core/routes/routes.dart';
import 'notifier/movies_notifier.dart';

class HomePage extends HookConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final movieStateNotifierProvider = movieStateNotifierProviderFamily(
      context,
    );

    final movieStateNotifier = ref.watch(movieStateNotifierProvider.notifier);

    useEffect(() {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        movieStateNotifier.fetchMovies();
      });

      return null;
    }, const []);

    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: AppConstraints.medium),
        children: [
          const _AppBarWidget(),
          const SizedBox(
            height: AppConstraints.lg,
          ),
          Text(
            'Recents',
            style: GoogleFonts.poppins(
              fontSize: AppConstraints.medium,
              color: AppColors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: AppConstraints.medium,
          ),
          Consumer(
            builder: (_, cRef, __) {
              final state = ref.watch(movieStateNotifierProvider);
              const double circularProgressIndicatorSize = 80;
              return state.maybeWhen(
                  loadSuccess: (list) {
                    return SizedBox(
                      height: 600,
                      child: ListView.builder(
                        padding: EdgeInsets.zero,
                        scrollDirection: Axis.horizontal,
                        itemCount: list.length,
                        itemBuilder: (_, index) {
                          final movie = list[index];
                          return GestureDetector(
                            onTap: () {
                              AppRoutes.details.push(
                                context,
                                arguments: DetailsPageArgs(movie: movie),
                              );
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Hero(
                                  tag: movie.id,
                                  child: Container(
                                    margin: const EdgeInsets.only(
                                      right: AppConstraints.xsmall,
                                    ),
                                    height: 300,
                                    width: 200,
                                    decoration: BoxDecoration(
                                      color: AppColors.white,
                                      image: DecorationImage(
                                        image: NetworkImage(
                                          movie.imgUrl,
                                        ),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(
                                          AppConstraints.medium,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: AppConstraints.small,
                                ),
                                Text(
                                  movie.title,
                                  style: GoogleFonts.poppins(
                                    color: AppColors.white,
                                    fontSize: AppConstraints.medium,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const SizedBox(
                                  height: AppConstraints.xxsmall,
                                ),
                                Text(
                                  'Created by: ${movie.director.name}',
                                  style: GoogleFonts.poppins(
                                    color: AppColors.white,
                                    fontSize: AppConstraints.small,
                                  ),
                                ),
                                const SizedBox(
                                  height: AppConstraints.xxsmall,
                                ),
                                Text(
                                  'Release Date: ${AppPipes.formatDate(DateTime.parse(movie.releaseDate))}',
                                  style: GoogleFonts.poppins(
                                    color: AppColors.white,
                                    fontSize: AppConstraints.xsmall,
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    );
                  },
                  loadInProgress: () {
                    return const SizedBox(
                      width: circularProgressIndicatorSize,
                      height: circularProgressIndicatorSize,
                      child: Center(
                        child:
                            CircularProgressIndicator(color: AppColors.white),
                      ),
                    );
                  },
                  loadFailure: (failure) {
                    switch (failure) {
                      //if you want to show different widgets or errors for user, just add more failures in failures file
                      case CommonFailures.unknown:
                      default:
                        return Container(
                          color: Colors.red,
                          height: 200,
                          width: 200,
                        );
                    }
                  },
                  orElse: SizedBox.shrink);
            },
          )
        ],
      ),
    );
  }
}

class _AppBarWidget extends StatelessWidget {
  const _AppBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Row(
        children: [
          const Icon(
            Icons.local_movies_sharp,
            color: AppColors.white,
            size: AppConstraints.xlg,
          ),
          const SizedBox(width: AppConstraints.small),
          Text(
            'CoolMovies',
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.bold,
              color: AppColors.white,
              fontSize: AppConstraints.xlg,
            ),
          )
        ],
      ),
    );
  }
}
