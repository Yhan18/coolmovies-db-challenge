import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../domain/entities/movies_entity.dart';
import '../../../../domain/services/movie_service.dart';
import '../../../../resources/failures/common_failures.dart';
import '../../../../resources/providers/service_providers.dart';
import '../../../core/common_state/common_state.dart';

typedef MovieState = CommonState<CommonFailures, List<MoviesEntity>>;

class MovieNotifier extends StateNotifier<MovieState> {
  final MovieService service;

  MovieNotifier(
    this.service,
  ) : super(const MovieState.initial());

  Future<void> fetchMovies() async {
    state = const MovieState.loadInProgress();

    final result = await service.fetchMovies();

    state = result.fold(
      MovieState.loadFailure,
      MovieState.loadSuccess,
    );
  }
}

final movieStateNotifierProviderFamily =
    StateNotifierProvider.family<MovieNotifier, MovieState, BuildContext>(
  (ref, context) => MovieNotifier(
    ref.watch(
      movieServiceProvider(GraphQLProvider.of(context).value),
    ),
  ),
);
