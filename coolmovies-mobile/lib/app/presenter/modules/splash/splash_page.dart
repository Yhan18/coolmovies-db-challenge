import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:lottie/lottie.dart';

import '../../../resources/notifier/user_notifier.dart';
import '../../../resources/shared/theme/app_animations.dart';
import '../../core/routes/routes.dart';

class SplashPage extends HookConsumerWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final userServiceStateNotifierProvider =
        userServiceStateNotifierProviderFamily(context);

    final userServiceNotifier =
        ref.watch(userServiceStateNotifierProvider.notifier);

    useEffect(() {
      Future.delayed(const Duration(seconds: 2), () {
        userServiceNotifier.setCurrentUser();
        AppRoutes.home.pushAndResetStack(context, arguments: noArgs);
      });
      return () {};
    }, const []);

    return Scaffold(
      body: Center(
        child: Lottie.asset(AppAnimations.splashAnimation),
      ),
    );
  }
}
