part of 'config.dart';

typedef PageBuilder = Widget Function(Object args);

abstract class RouteTransitions {
  Duration get transitionDuration;
  Duration get reverseTransitionDuration;
  Widget transitionsBuilder(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  );
}

class AppPageRoute<ArgType> {
  final String path;

  /// Warning: Used to register the route. You must not use this to navigate
  final PageBuilder pageBuilder;
  final RouteTransitions? routeTransitions;
  final bool useRootNavigation;

  const AppPageRoute({
    required this.path,
    required this.pageBuilder,
    this.useRootNavigation = true,
    this.routeTransitions,
  });
}
