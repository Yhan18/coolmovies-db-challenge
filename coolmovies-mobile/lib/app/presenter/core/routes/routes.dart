import '../../modules/home/home_page.dart';
import '../../modules/movie_details/movie_details_page.dart';
import '../../modules/splash/splash_page.dart';
import '../args/details_page_args.dart';
import '../router/config.dart';

export '../router/config.dart' show PageRouteNavigation, noArgs;

abstract class AppRoutes {
  static final splash = AppPageRoute<NoArgs>(
    path: '/',
    pageBuilder: (_) => const SplashPage(),
  );
  static final home = AppPageRoute<NoArgs>(
    path: '/home',
    pageBuilder: (_) => const HomePage(),
  );

  static final details = AppPageRoute<DetailsPageArgs>(
    path: '/details',
    pageBuilder: (args) => MovieDetailsPage(
      args: args as DetailsPageArgs,
    ),
  );

  static final routes = routeMapFrom([
    splash,
    home,
    details,
  ]);
}
