import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../resources/shared/theme/app_theme.dart';
import 'router/config.dart';
import 'routes/routes.dart';

class AppWidget extends HookConsumerWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ProviderScope(
      child: MaterialApp(
        title: 'CoolMovies',
        debugShowCheckedModeBanner: false,
        // Uncomment this lines to add localization support to Brazil, if you want other countries just add then in supportedLocales
        // localizationsDelegates: GlobalMaterialLocalizations.delegates,
        // supportedLocales: [
        //   const Locale('pt', 'BR'),
        // ],
        // locale: const Locale('pt', 'BR'),
        theme: AppTheme.primary,
        initialRoute: AppRoutes.splash.path,
        onGenerateRoute: AppRouter(AppRoutes.routes).onGenerateRoutes,
      ),
    );
  }
}
