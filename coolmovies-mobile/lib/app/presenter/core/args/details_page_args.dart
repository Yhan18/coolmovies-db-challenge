import '../../../domain/entities/movies_entity.dart';

class DetailsPageArgs {
  final MoviesEntity movie;

  DetailsPageArgs({
    required this.movie,
  });
}
