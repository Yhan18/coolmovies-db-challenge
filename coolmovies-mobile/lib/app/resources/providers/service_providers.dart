import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../domain/services/movie_service.dart';
import '../../domain/services/user_service.dart';
import '../../infra/services/movie_service_impl.dart';
import '../../infra/services/user_service_impl.dart';

final movieServiceProvider = Provider.family<MovieService, GraphQLClient>(
  (ref, client) => MovieServiceImpl(client),
);

final userServiceProvider = Provider.family<UserService, GraphQLClient>(
  (ref, client) => UserServiceImpl(client: client),
);
