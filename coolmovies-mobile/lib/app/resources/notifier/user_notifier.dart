import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../domain/services/user_service.dart';
import '../providers/service_providers.dart';

class UserServiceNotifier extends StateNotifier<String> {
  final UserService service;
  UserServiceNotifier(this.service) : super('');

  Future<void> setCurrentUser() async {
    await service.setCurrentUser();
  }

  Future<void> getCurrentUser() async {
    final result = await service.getCurrentUser();

    result.fold(() => null, (id) => state = id);
  }
}

final userServiceStateNotifierProviderFamily =
    StateNotifierProvider.family<UserServiceNotifier, String, BuildContext>(
  (ref, context) => UserServiceNotifier(
    ref.watch(
      userServiceProvider(GraphQLProvider.of(context).value),
    ),
  ),
);
