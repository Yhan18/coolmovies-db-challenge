class AppAnimations {
  static const path = 'assets/animations/lottie';
  static const splashAnimation = '$path/splash_animation.json';
}
