class AppConstraints {
  static const double xxxsmall = 2;
  static const double xxsmall = 4;
  static const double xsmall = 6;
  static const double small = 8;
  static const double medium = 12;
  static const double normal = 16;
  static const double lg = 24;
  static const double xlg = 32;
  static const double xxlg = 48;
  static const double xxxxlg = 64;
}
