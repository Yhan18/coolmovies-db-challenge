import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xFF22223b);
  static const Color secondaryColor = Color(0xFF4a4e69);
  static const Color contrast = Color(0xFFfca311);
  static const Color white = Colors.white;
  static const Color black = Colors.black;
}
