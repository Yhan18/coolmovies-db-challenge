
import 'package:intl/intl.dart';

class AppPipes {
  static String formatDate(DateTime date, {String format = 'EEEE, MMM d, yyyy'}) {
    final formatter = DateFormat(format);
    return formatter.format(date);
  }
  }