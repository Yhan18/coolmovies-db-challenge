import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../shared/theme/app_colors.dart';
import '../shared/theme/app_constraints.dart';

abstract class TemplateModal extends HookConsumerWidget {
  final bool barrierDismissible;

  const TemplateModal({
    Key? key,
    this.barrierDismissible = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return AlertDialog(
      backgroundColor: Colors.white,
      contentPadding: const EdgeInsets.all(AppConstraints.small),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(AppConstraints.medium)),
      ),
      elevation: 0,
      content: WillPopScope(
        onWillPop: () async => barrierDismissible,
        child: DefaultTextStyle(
          style: const TextStyle(),
          textAlign: TextAlign.center,
          child: buildContent(context, ref),
        ),
      ),
    );
  }

  Widget buildContent(BuildContext context, WidgetRef ref);

  Future<T?> show<T>(BuildContext context) {
    return showDialog<T>(
      context: context,
      builder: (context) => this,
      barrierDismissible: barrierDismissible,
      barrierColor: AppColors.secondaryColor.withOpacity(0.3),
    );
  }
}
