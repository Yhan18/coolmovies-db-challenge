import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../shared/theme/app_colors.dart';
import '../shared/theme/app_constraints.dart';

abstract class TemplateBottomSheet extends HookConsumerWidget {
  final bool dismissible;
  const TemplateBottomSheet({super.key, this.dismissible = true});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    const contentPadding = EdgeInsets.symmetric(
      vertical: AppConstraints.normal,
      horizontal: AppConstraints.medium,
    );

    final windowMediaQuery =
        MediaQueryData.fromWindow(WidgetsBinding.instance.window);

    // ignore: no_leading_underscores_for_local_identifiers
    final _contentPadding = contentPadding.copyWith(
      top: AppConstraints.small,
      bottom: contentPadding.bottom,
    );

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      curve: Curves.decelerate,
      constraints: BoxConstraints(
        maxHeight: windowMediaQuery.size.height * .9,
      ),
      decoration: const BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(AppConstraints.medium),
          topRight: Radius.circular(AppConstraints.medium),
        ),
      ),
      padding: _contentPadding.add(
        EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
      ),
      child: SafeArea(
        top: false,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: AppColors.secondaryColor,
              ),
              height: 5,
              width: 32,
            ),
            const SizedBox(
              height: AppConstraints.medium,
            ),
            buildContent(context, ref),
          ],
        ),
      ),
    );
  }

  Widget buildContent(BuildContext context, WidgetRef ref);

  Future<T?> show<T>(BuildContext context) {
    return showModalBottomSheet<T>(
      context: context,
      builder: (_) => this,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(
            AppConstraints.normal,
          ),
        ),
      ),
    );
  }
}
