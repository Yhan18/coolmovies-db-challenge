import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'template_config.dart';

class Config extends TemplateConfig {
  Config._();

  static final instance = Config._();

  late Link? graphqlLink;
  late ValueNotifier<GraphQLClient>? graphQlClient;

  @override
  void configGraphQlLink() {
    final HttpLink httpLink = HttpLink(
      Platform.isAndroid
          ? 'http://10.0.2.2:5001/graphql'
          : 'http://localhost:5001/graphql',
    );

    final AuthLink authLink = AuthLink(
      getToken: () async => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
    );

    graphqlLink = authLink.concat(httpLink);
  }

  @override
  Future<void> configHive() async {
    await initHiveForFlutter();
  }

  @override
  void setupGraphQlClient() {
    graphQlClient = ValueNotifier(
      GraphQLClient(
        link: graphqlLink!,
        cache: GraphQLCache(store: HiveStore()),
      ),
    );
  }
}
