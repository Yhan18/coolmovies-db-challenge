abstract class TemplateConfig {
  void configGraphQlLink();

  Future<void> configHive();

  void setupGraphQlClient();

  Future<void> call() async {
    await configHive();
    configGraphQlLink();
    setupGraphQlClient();
  }
}
